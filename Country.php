<?php

  class Country{
    private $id;
    private $country_name;

    function setId($id) {
      $this->id = $id;
      return $this;
    }

    function getId() {
      return $this->id;
    }

    function setCountry_name($country_name) {
      $this->country_name = $country_name;
      return $this;
    }

    function getCountry_name() {
      return $this->country_name;
    }

    public function __construct($id,$country_name){
      $this->setId($id);
      $this->setCountry_name($country_name);
    }

  }

?>
